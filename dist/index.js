
'use strict'

if (process.env.NODE_ENV === 'production') {
  module.exports = require('./worstdb.cjs.production.min.js')
} else {
  module.exports = require('./worstdb.cjs.development.js')
}
