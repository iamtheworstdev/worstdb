import { format } from 'date-fns';

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _objectWithoutPropertiesLoose(source, excluded) {
  if (source == null) return {};
  var target = {};
  var sourceKeys = Object.keys(source);
  var key, i;

  for (i = 0; i < sourceKeys.length; i++) {
    key = sourceKeys[i];
    if (excluded.indexOf(key) >= 0) continue;
    target[key] = source[key];
  }

  return target;
}

var Models =
/*#__PURE__*/
function () {
  function Models(ddb, resourceName, timestamp, relationships) {
    this.resourceName = 'model';
    this.ddb = ddb;
    this.resourceName = resourceName;
    this.timestamp = timestamp;
    this.relationships = relationships;
  }

  var _proto = Models.prototype;

  _proto.getPrimaryKey = function getPrimaryKey(id) {
    var _pk;

    var pk = (_pk = {}, _pk[this.ddb.partitionKey] = this.getIdWithPrefix(id), _pk[this.ddb.sortKey] = this.resourceName, _pk);
    console.log("pk = " + JSON.stringify(pk));
    return pk;
  };

  _proto.getIdWithPrefix = function getIdWithPrefix(id) {
    var prefix = this.resourceName + "#";
    return id.startsWith(prefix) ? id : "" + prefix + id;
  };

  _proto.stripIdPrefix = function stripIdPrefix(id) {
    if (id.match(/^#/)) {
      return id;
    }

    return id.substring(id.indexOf('#') + 1);
  };

  _proto.itemToModel = function itemToModel(item) {
    if (!item) {
      return undefined;
    }

    var model = _extends({
      id: this.stripIdPrefix(item[this.ddb.partitionKey])
    }, item);

    if (this.timestamp) {
      model.created_at = model[this.timestamp.sortKey];
      delete model[this.timestamp.sortKey];
    }

    delete model['pk'];
    delete model['sk'];
    return model;
  };

  _proto.getKeyValue = function getKeyValue(value) {
    if (typeof value === 'function') {
      return value();
    }

    return value;
  };

  _proto.create = function create(model) {
    try {
      var _this2 = this;

      var id = model.id,
          modelAttributes = _objectWithoutPropertiesLoose(model, ["id"]);

      var item = _extends({}, _this2.getPrimaryKey(id), {}, modelAttributes);

      if (_this2.timestamp) {
        item["" + _this2.timestamp.sortKey] = _this2.ddb.getCurrentTimestamp();
      }

      console.log("create - " + JSON.stringify(item));
      return Promise.resolve(_this2.save(item));
    } catch (e) {
      return Promise.reject(e);
    }
  };

  _proto.save = function save(Item) {
    try {
      var _this4 = this;

      console.log("save - " + JSON.stringify(Item));
      return Promise.resolve(_this4.ddb.client.put({
        TableName: _this4.ddb.tableName,
        Item: Item
      }).promise());
    } catch (e) {
      return Promise.reject(e);
    }
  };

  _proto.get = function get(id) {
    try {
      var _this6 = this;

      console.log("get id - " + JSON.stringify(id));
      return Promise.resolve(_this6.ddb.client.get({
        TableName: _this6.ddb.tableName,
        Key: _this6.getPrimaryKey(id)
      }).promise()).then(function (_ref) {
        var Item = _ref.Item;
        console.log("get " + JSON.stringify(Item));
        return _this6.itemToModel(Item);
      });
    } catch (e) {
      return Promise.reject(e);
    }
  };

  _proto.getNodes = function getNodes(model, partitionKeyValue, sortKeyValue) {
    try {
      var _this8 = this;

      if (!_this8.relationships || !_this8.relationships[model]) {
        throw new Error('Relationship not specified');
      }

      var count = 10;
      var r = _this8.relationships[model];

      var pKey = _this8.getKeyValue(partitionKeyValue) || _this8.getKeyValue(r.partitionKeyValue) || _this8.resourceName;

      var sKey = _this8.getKeyValue(sortKeyValue) || _this8.getKeyValue(r.sortKeyValue) || model;
      return Promise.resolve(_this8.ddb.client.query({
        TableName: _this8.ddb.tableName,
        IndexName: r.index,
        Limit: count,
        KeyConditionExpression: r.partitionKey + " = :pKey and " + r.sortKey + " = :sKey",
        ExpressionAttributeValues: {
          ':pKey': pKey,
          ':sKey': sKey
        },
        ScanIndexForward: false
      }).promise()).then(function (result) {
        return result.Count && result.Items ? result.Items.map(function (item) {
          return _this8.itemToModel(item);
        }) : [];
      });
    } catch (e) {
      return Promise.reject(e);
    }
  };

  _proto.getRecent = function getRecent(count) {
    try {
      var _this10 = this;

      if (!_this10.timestamp) {
        throw new Error('Model does not support getRecent');
      }

      console.log("ts index - " + JSON.stringify(_this10.timestamp));
      var query = {
        TableName: _this10.ddb.tableName,
        IndexName: _this10.timestamp.index,
        Limit: count,
        KeyConditionExpression: "sk = :hkey",
        ExpressionAttributeValues: {
          ':hkey': _this10.resourceName
        },
        ScanIndexForward: false
      };
      console.log("query input - " + JSON.stringify(query));
      return Promise.resolve(_this10.ddb.client.query(query).promise()).then(function (result) {
        return result.Count && result.Items ? result.Items.map(function (item) {
          return _this10.itemToModel(item);
        }) : [];
      });
    } catch (e) {
      return Promise.reject(e);
    }
  };

  return Models;
}();

var DDB =
/*#__PURE__*/
function () {
  function DDB(client, tableName, partitionKey, sortKey) {
    this.client = client;
    this.tableName = tableName;
    this.partitionKey = partitionKey;
    this.sortKey = sortKey;
    this.models = {};
  }

  var _proto = DDB.prototype;

  _proto.addGenericModel = function addGenericModel(name, timestampIndex) {
    if (timestampIndex) {
      var timestamp = {
        index: timestampIndex,
        partitionKey: 'sk',
        sortKey: timestampIndex + "sk"
      };
      this.models[name] = new Models(this, name, timestamp);
    } else {
      this.models[name] = new Models(this, name);
    }
  };

  _proto.addModel = function addModel(name, model) {
    this.models[name] = model;
  };

  _proto.getCurrentTimestamp = function getCurrentTimestamp() {
    return format(new Date(), "yyyy-MM-dd'T'HH:mm:ss.SSSxxx");
  };

  return DDB;
}();

export { DDB, Models };
//# sourceMappingURL=worstdb.esm.js.map
