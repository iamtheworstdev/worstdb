import { DDB } from './index';
import { DocumentClient } from 'aws-sdk/clients/dynamodb';
declare type Function = () => string | number;
export declare type Model = {
    id: string;
} & Record<string, any>;
export declare type Index = {
    index: string;
    partitionKey: string;
    partitionKeyValue?: string;
    sortKey: string;
    sortKeyValue?: string;
};
export declare type Relationship = Index & {
    type: 'Many_to_Many' | '1_to_1' | '1_to_many' | 'Many_to_1';
};
declare type Relationships = Record<string, Relationship>;
declare type ModelList = (Model | undefined)[];
export declare class Models {
    ddb: DDB;
    resourceName: string;
    timestamp: Index | undefined;
    relationships: Relationships | undefined;
    constructor(ddb: DDB, resourceName: string, timestamp?: Index, relationships?: Relationships);
    getPrimaryKey(id: string): {
        [x: string]: string;
    };
    getIdWithPrefix(id: string): string;
    stripIdPrefix(id: string): string;
    itemToModel(item: DocumentClient.AttributeMap | undefined): Model | undefined;
    getKeyValue(value: number | string | Function | undefined): number | string | undefined;
    create(model: Model): Promise<import("aws-sdk/lib/request").PromiseResult<DocumentClient.PutItemOutput, import("aws-sdk/lib/error").AWSError>>;
    save(Item: DocumentClient.PutItemInputAttributeMap): Promise<import("aws-sdk/lib/request").PromiseResult<DocumentClient.PutItemOutput, import("aws-sdk/lib/error").AWSError>>;
    get(id: string): Promise<Model | undefined>;
    getNodes(model: string, partitionKeyValue?: number | string | Function, sortKeyValue?: number | string | Function): Promise<ModelList>;
    getRecent(count: number): Promise<ModelList>;
}
export {};
