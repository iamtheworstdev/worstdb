import { DocumentClient } from 'aws-sdk/clients/dynamodb';
import { Models } from './model';
export declare class DDB {
    client: DocumentClient;
    tableName: string;
    partitionKey: string;
    sortKey: string;
    models: Record<string, Models>;
    constructor(client: DocumentClient, tableName: string, partitionKey: string, sortKey: string);
    addGenericModel(name: string, timestampIndex?: string): void;
    addModel(name: string, model: Models): void;
    getCurrentTimestamp(): string;
}
